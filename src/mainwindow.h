#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QFileDialog>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void handleAddFiles();
    void handleAddLogo();
    void handleChangeLogo();
    void handleChangeFolderPath();
    void handleClearPath();
    void setLogo(QString);
    void disableAllButtons(bool);
private:
    Ui::MainWindow *ui;

    QFileDialog dialog;
    //QFileDialog *dialog;
    //QPixmap *logoPixmap;
    QString logoPath;
    QString outputPath;
};

#endif // MAINWINDOW_H
