#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QDir>
#include <QPainter>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    dialog.setViewMode(QFileDialog::Detail);
    dialog.setDirectory(QDir::currentPath());

    QString lPath = QDir::currentPath() + "/logo.png";
    outputPath = QDir::currentPath() + "/output/";

    ui->outputFolderPathLineEdit->setReadOnly(true);
    ui->outputFolderPathLineEdit->setText(outputPath);

    if (!QDir(outputPath).exists())
        QDir().mkdir(outputPath);

    setLogo(lPath);


    ui->outputFolderPathLineEdit->setText(outputPath);

    connect(ui->addFilesButton, SIGNAL(clicked()),
            this, SLOT(handleAddFiles()));

    connect(ui->convertFilesButton, SIGNAL(clicked()),
            this, SLOT(handleAddLogo()));

    connect(ui->folderButton, SIGNAL(clicked()),
            this, SLOT(handleChangeFolderPath()));

    connect(ui->logoButton, SIGNAL(clicked()),
            this, SLOT(handleChangeLogo()));

    connect(ui->clearListButton, SIGNAL(clicked()),
            this, SLOT(handleClearPath()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handleAddFiles()
{
    QStringList fileNames;

    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setNameFilter(tr("Images (*.png *.jpg)"));
    if (dialog.exec())
        fileNames = dialog.selectedFiles();

    ui->fileListWidget->addItems(fileNames);
}

void MainWindow::handleAddLogo()
{
    QImage logoImg(logoPath);
    if (logoImg.isNull())
        return;

    logoImg = logoImg.scaledToWidth(640);

    for (int i = 0; i < ui->fileListWidget->count(); i++)
    {
        QString itemFilePath = ui->fileListWidget->item(i)->text();
        QImage img(itemFilePath);

        if (img.isNull())
            continue;

        img = img.scaledToWidth(640);

        QImage targetImage = QImage(img.size(), QImage::Format_ARGB32_Premultiplied);
        QPainter painter(&targetImage);

        painter.setCompositionMode(QPainter::CompositionMode_Source);
        painter.fillRect(img.rect(), Qt::transparent);

        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
        painter.drawImage(0, 0, img);

        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
        painter.drawImage(0, 0, logoImg);

        painter.end();

        QFileInfo info(itemFilePath);

        QString name = outputPath + info.fileName();
        targetImage.save(name);

        targetImage = QImage();
        img = QImage();
    }

    handleClearPath();
}

void MainWindow::handleChangeFolderPath()
{
    qDebug()<<"handleChangeFolderPath"<<endl;
}

void MainWindow::handleChangeLogo()
{
    qDebug()<<"handleChangeLogo"<<endl;
}

void MainWindow::handleClearPath()
{
    ui->fileListWidget->clear();
}

void MainWindow::setLogo(QString path)
{
    QPixmap *pmap = new QPixmap(path);

    if (pmap->isNull())
        return;

    ui->logoLabel->setPixmap(pmap->scaled(128, 96));
    ui->logoFolderLineEdid->setText(path);
    logoPath = path;
}

void MainWindow::disableAllButtons(bool state)
{
    ui->addFilesButton->setEnabled(!state);
    ui->convertFilesButton->setEnabled(!state);
    ui->folderButton->setEnabled(!state);
    ui->logoButton->setEnabled(!state);
    ui->clearListButton->setEnabled(!state);
    ui->outputFolderPathLineEdit->setEnabled(!state);
    ui->logoFolderLineEdid->setEnabled(!state);
}
